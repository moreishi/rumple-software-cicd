<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Gig;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Gig::class, function (Faker $faker) {

    $title = $faker->sentence;

    return [
        'title' => $title,
        'content' => $faker->paragraph(3),
        'short_description' => $faker->paragraph(1),
        'description' => $faker->paragraph(2),
        'user_id' => null,
        'slug' => Str::slug($title)
    ];
});
