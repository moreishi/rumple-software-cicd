<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;
use App\Gig;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1000)->create();

        $users = User::all();

        foreach($users as $user)
        {
            $user->avatar()->create(['url' => 'https://i.pravatar.cc/150?u=' . Carbon::now()->timestamp]);
        }
    }
}
