<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Gig;

class LikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gigs = Gig::all();
        //$gigs = Gig::all();
        // $users_count = User::limit()->count();
        $users_count = 100;
        $gigs = Gig::limit(100)->get();

        foreach($gigs as $gig)
        {
            $random_likes = rand(1,$users_count);

            for($i = 0; $i < $random_likes; $i++)
            {
                $gig->likes()->create(['user_id' => $i]);
            }
        }
    }
}
