<?php

use Illuminate\Database\Seeder;
use App\Gig;
use App\User;

class ParticipantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $users_count = User::count();
        $users_count = rand(10,100);
        // $gigs = Gig::all();
        $gigs = Gig::limit(100)->get();

        foreach($gigs as $gig)
        {
            for($i = 1; $i < $users_count; $i++)
            {
                $gig->participants()->create(['user_id' => $i]);
            }
        }
    }
}
