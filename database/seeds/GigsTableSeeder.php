<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Gig;
use Carbon\Carbon;

class GigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $scheduled_at = Carbon::today()->addDays(rand(3,4))->addHours(22);

        foreach($users as $user)
        {
            factory(Gig::class,rand(1,5))->create([
                'scheduled_at' => $scheduled_at,
                'expired_at' => $scheduled_at,
                'user_id' => $user->id
            ]);
        }
    }
}
