@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <h1 class="mb-5">Create A Gig</h1>

                <create-auction></create-auction>

            </div>
        </div>
    </div>

@endsection
