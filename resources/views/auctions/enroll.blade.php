@extends('layouts.app')

@section('content')

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Fluid jumbotron</h1>
            <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>

            <form class="form-inline">
                <div class="row">
                    <div class="form-group mx-sm-3 mb-2 mr-3">
                        <label for="inputPassword2" class="sr-only">Enter voucher</label>
                        <input type="password" class="form-control" id="inputPassword2" placeholder="Password">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mb-2"></button>
            </form>
        </div>
    </div>

    <div class="container">

    </div>

@endsection
