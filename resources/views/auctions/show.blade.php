@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">

            <div class="row">

                <div class="col-md-8">
                    <h1 class="product-title">{{$gig->title}}</h1>
                    <div class="card">
                        <img src="https://picsum.photos/200?grayscale" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p class="card-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequun</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">

                    <last-bidder></last-bidder>

                    <div class="card mb-4">
                        <bid auction-id="{{ $gig->id }}" auction-data="{{ $gig }}"></bid>
                    </div>

                    @include('components.participants.participants')


                </div>
            </div>

        </div>
    </div>

@endsection
