@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @foreach($auctions as $auction)
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">{{$auction->title}}</h5>
{{--                            <h6 class="card-subtitle mb-2 text-muted">{{$auction->expired_at}}</h6>--}}
                            <p class="card-text">{{$auction->description}}</p>
                            <a href="/auctions/{{$auction->slug}}" class="card-link stretched-link"></a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection
