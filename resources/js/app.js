/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuex from 'vuex'
import combined_store from './store/store';
import { Form, HasError, AlertError } from 'vform'

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.use(Vuex)

window.Form = Form;


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import "@fortawesome/fontawesome-free/css/all.css";

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('auction-timer', require('./components/AuctionTimer.vue').default);
Vue.component('Bid', require('./components/Bid/Bid.vue').default);
Vue.component('Points', require('./components/Bid/Points.vue').default);
Vue.component('ItemPrice', require('./components/Bid/ItemPrice.vue').default);
Vue.component('ItemRetailPrice', require('./components/Bid/ItemRetailPrice.vue').default);

// Bidder
Vue.component('LastBidder', require('./components/Bid/LastBidder').default);

// Auction
Vue.component('CreateAuction', require('./components/Auction/CreateAuction/CreateAuction').default);

/**
 * State
 */

const store = new Vuex.Store(combined_store);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store
});
