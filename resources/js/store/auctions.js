const auctions = {
    state: () => ({
        item_price: 0,
        item_retail_price: 0,
        created_at: '',
        modified_at: '',
        scheduled_at: '',
        expired_at: '',
        last_bidder: {}
    }),
    mutations: {
        place_bid: (state, payload) => {
            state.item_price = payload.amount;
        },
        update_item_price: (state, payload) => {
            state.item_price = payload.amount;
        }
    },
    actions: {},
    getters: {
        get_item_price: (state, getters) => {
            return state.item_price;
        },
        get_item_retail_price: (state, getters) => {
            return state.item_retail_price;
        }
    }
}

export default auctions;
