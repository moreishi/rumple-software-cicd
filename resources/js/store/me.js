const me = {
    state: () => ({
        bid_points: 0
    }),
    mutations: {
        bid_points_decrement: (state, payload) => {
            state.bid_points -= payload.amount;
        }
    },
    actions: {},
    getters: {
        get_bid_points: (state, getters) => {
            return state.bid_points;
        }
    }
}

export default me;
