import me from './me';
import auction from './auction';

const store = {
    modules: {
        myAccount: me,
        currentAuction: auction
    }
}

export default store;
