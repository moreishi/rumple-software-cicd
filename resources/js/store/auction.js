const auction = {
    state: () => ({
        item_price: 0,
        item_retail_price: 0,
        created_at: '',
        modified_at: '',
        scheduled_at: '',
        expired_at: '00:00:00',
        last_bidder: {
            id: null,
            name: null,
            avatar_url: null
        }
    }),
    mutations: {
        place_bid: (state, payload) => {
            state.item_price = payload.amount;
        },
        update_item_price: (state, payload) => {
            state.item_price = payload.amount;
        },
        update_last_bidder: (state, payload) => {
            console.log(payload);
            state.last_bidder = payload.user;
        },
        update_auction_expiry: (state, payload) => {
            state.expired_at = payload.expired_at;
        }
    },
    actions: {},
    getters: {
        get_item_price: (state) => {
            return state.item_price;
        },
        get_item_retail_price: (state) => {
            return state.item_retail_price;
        },
        get_last_bidder: (state) => {
            return state.last_bidder;
        },
        get_auction_expiry: (state) => {
            return state.expired_at;
        }
    }
}

export default auction;
