<?php

namespace App\Http\Controllers\Api;

use App\Events\BidEvent;
use App\Gig;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bid;
use App\User;
use Carbon\Carbon;

class BidController extends Controller
{
    public $bid;

    public $me;

    /**
     * BidController constructor.
     * @param Bid $bid
     */
    public function __construct(Bid $bid)
    {
        $this->bid = $bid;

        $this->me = auth('api')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @param $gigId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request,$gigId)
    {
        $gig = Gig::find($gigId);
        $gig->current_bid += (int) 1;
        $gig->expired_at = Carbon::now()->addSeconds(11);
        $bid = $gig->bids()->create([
            'user_id' => auth('api')->user()->id,
            'price' => $gig->current_bid,
        ]);
        $gig->save();
        $gig->refresh();
        $bid->refresh();

        $avatar_url = User::with('avatar')->where('id',$this->me->id)->first()->avatar->url;

        event(new BidEvent([
            'bid' => ['current_bid' => $bid->price],
            'gig' => ['id' => $gig->id,'expired_at' => $gig->expired_at],
            'user'=> ['id' => $this->me->id, 'name' => $this->me->name, 'avatar_url' => $avatar_url]
        ]));

        return response()->json([
            'status' => 'Ok',
            'result' => compact('bid','gig')
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBidsByGigId($id)
    {
        // $gig =
        return response()->json([], 200);
    }
}
