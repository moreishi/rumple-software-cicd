<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Gig;
use App\Bid;
use App\Participant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class GigController extends Controller
{
    public $gig;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(Gig $gig)
    {
        $this->gig = $gig;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gigs = $this->gig->with(['user' => function($q) {
            return $q->select('id','username')->with('avatar');
        }])->withCount(['participants','likes','views'])->paginate(10);

        return response()->json([
            'status' => 'Ok',
            'result' => compact('gigs')
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validate = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
            'short_description' => 'required',
            'scheduled_at' => 'required'
        ]);

        if($validate->fails()) return response()->json([
            'status' => 'Failed',
            'result' => ['error' => $validate->errors()]
        ], 400);

        $gig = Gig::create([
            'title' => $request->title,
            'content' => $data['content'],
            'short_description' => $request->short_description,
            'scheduled_at' => Carbon::parse($request->scheduled_at)->format('Y-m-d H:i:s'),
            'user_id' => auth('api')->user()->id
        ]);
        $gig->refresh();

        return response()->json([
            'status' => 'Ok',
            'result' => compact('gig')
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gig = $this->gig->with(['user' => function($q) {
            return $q->select('id','username')->with('avatar');
        }])->where('id',$id)->first();

        return response()->json([
            'status' => 'Ok',
            'result' => compact('gig')
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validate = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
            'short_description' => 'required',
            'scheduled_at' => 'required'
        ]);

        if($validate->fails()) return response()->json([
            'status' => 'Failed',
            'result' => ['error' => $validate->errors()]
        ], 400);

        $gig = Gig::where('id', $id)->update([
            'title' => $request->title,
            'content' => $data['content'],
            'short_description' => $request->short_description,
            'scheduled_at' => Carbon::parse($request->scheduled_at)->format('Y-m-d H:i:s'),
            'user_id' => auth('api')->user()->id
        ]);

        $gig = Gig::find($id);

        return response()->json([
            'status' => 'Ok',
            'result' => compact('gig')
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!Gig::destroy($id)) {
            return response()->json([
                'status' => 'Failed',
                'result' => [
                    'message' => 'Unable to delete record.'
                ]
            ], 422);
        }

        return response()->json([
            'status' => 'Ok',
            'result' => [
                'message' => 'Record has been deleted.'
            ]
        ], 200);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGigsByUserId($userId)
    {
        $gigs = $this->gig->with(['user' => function($q) {
            return $q->select('id','username')->with('avatar');
        }])->where('user_id',$userId)->paginate(10);

        return response()->json([
            'status' => 'Ok',
            'result' => compact('gigs')
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function participants($id)
    {
        $participants = Participant::with(['user' => function($q) {
            return $q->select('id','username')->with('avatar');
        }])->where('participantable_id',$id)->paginate(10);

        return response()->json([
            'status' => 'Ok',
            'result' => compact('participants')
        ], 200);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function bids($id)
    {
        $bids = Bid::with(['user' => function($q) {
            return $q->select('id','username')->with('avatar');
        }])->where('bidable_id',$id)->paginate(10);

        return response()->json([
            'status' => 'Ok',
            'result' => compact('bids')
        ], 200);
    }
}
