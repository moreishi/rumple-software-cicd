<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;

    /**
     * Making all columns writable.
     * @var array
     */
    protected $guarded = [];

    /**
     * Show only these fields.
     * @var array
     */
    protected $visible = ['id','url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}