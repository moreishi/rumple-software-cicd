<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gig extends Model
{
    use SoftDeletes;

    /**
     * Unguard all attributes if empty.
     * @var array
     */
    protected $guarded = [];

    /**
     * Hide attributes from json response.
     * @var array
     */
    protected $hidden = ['user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function participants()
    {
        return $this->morphOne(Participant::class, 'participantable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function likes()
    {
        return $this->morphOne(Like::class, 'likeable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function views()
    {
        return $this->morphOne(View::class, 'viewable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function bids()
    {
        return $this->morphOne(Bid::class, 'bidable');
    }
}
