<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\PassportController@login');
Route::post('register', 'Api\PassportController@register');

Route::middleware('auth:api')->group(function() {

    Route::get('/users/{userId}/gigs', 'Api\GigController@getGigsByUserId');

    Route::get('/gigs/{gigId}/participants', 'Api\GigController@participants');

    Route::get('/gigs/{gigId}/bids', 'Api\GigController@bids');
    Route::post('/gigs/{gigId}/bids', 'Api\BidController@store');

    Route::resource('users', 'Api\UserController');
    Route::resource('gigs', 'Api\GigController');
    Route::resource('participants', 'Api\ParticipantController');
    Route::resource('bids', 'Api\BidController');
});