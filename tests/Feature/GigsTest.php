<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Gig;

class GigsTest extends Common
{
    /**
     * Get Paginated Gigs by User Id.
     */
    public function testGetGigsByUserId()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $response = $this->RequestApi('GET',
            '/api/users/' . $user->id . '/gigs',$token);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('gigs', $result->result);
    }

    /**
     * Get the my all gigs
     */
    public function testGetGigsOfLoggedInUser()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $response = $this->RequestApi('GET',
            '/api/gigs',$token);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('gigs', $result->result);
    }

    /**
     * Create a sample Gig record while logged in.
     */
    public function testCreateAGig()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $response = $this->RequestApi('POST',
            '/api/gigs',$token,[
            'title' => Str::random(16),
            'content' => Str::random(16),
            'short_description' => Str::random(16),
            'scheduled_at' => Carbon::today()->format('Y-m-d')
        ]);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('gig', $result->result);
        $this->assertObjectHasAttribute('id', $result->result->gig);
        $this->assertObjectHasAttribute('title', $result->result->gig);
        $this->assertObjectHasAttribute('content', $result->result->gig);
        $this->assertObjectHasAttribute('short_description', $result->result->gig);
        $this->assertObjectHasAttribute('scheduled_at', $result->result->gig);
    }

    /**
     * Test update gig by id
     * @test
     */
    public function testUpdateAGigById()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        /* Get use gig */
        $gig = $this->RequestApi('POST',
            '/api/gigs',$token,[
            'title' => Str::random(16),
            'content' => Str::random(16),
            'short_description' => Str::random(16),
            'scheduled_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

        $gig = json_decode($gig->getContent());

        $response = $this->RequestApi('PUT',
            '/api/gigs/' . $gig->result->gig->id,$token,[
                'title' => Str::random(16),
                'content' => Str::random(16),
                'short_description' => Str::random(16),
                'scheduled_at' => Carbon::now()->addSeconds(rand(1,10))->format('Y-m-d H:i:s')
            ]);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('gig', $result->result);
        $this->assertObjectHasAttribute('id', $result->result->gig);
        $this->assertObjectHasAttribute('title', $result->result->gig);
        $this->assertObjectHasAttribute('content', $result->result->gig);
        $this->assertObjectHasAttribute('short_description', $result->result->gig);
        $this->assertObjectHasAttribute('scheduled_at', $result->result->gig);

        // Check the response values
        $this->assertEquals($gig->result->gig->id, $result->result->gig->id);
        $this->assertNotEquals($gig->result->gig->title, $result->result->gig->title);
        $this->assertNotEquals($gig->result->gig->content, $result->result->gig->content);
        $this->assertNotEquals($gig->result->gig->short_description, $result->result->gig->short_description);
        $this->assertNotEquals($gig->result->gig->scheduled_at, $result->result->gig->scheduled_at);
    }

    /**
     * Test update gig by id
     * @test
     */
    public function testGetAGigById()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        /* Get use gig */

        $this->RequestApi('POST',
            '/api/gigs',$token,[
                'title' => Str::random(16),
                'content' => Str::random(16),
                'short_description' => Str::random(16),
                'scheduled_at' => Carbon::today()->format('Y-m-d')
            ]);

        $gig = Gig::first();

        $response = $this->RequestApi('GET',
            '/api/gigs/' . $gig->id,$token);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('gig', $result->result);
        $this->assertObjectHasAttribute('id', $result->result->gig);
        $this->assertObjectHasAttribute('title', $result->result->gig);
        $this->assertObjectHasAttribute('content', $result->result->gig);
        $this->assertObjectHasAttribute('short_description', $result->result->gig);
        $this->assertObjectHasAttribute('scheduled_at', $result->result->gig);
    }

    /**
     * Test update gig by id
     * @test
     */
    public function testDeleteAGigById()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        /* Get use gig */
        $this->RequestApi('POST',
        '/api/gigs/',$token,[
            'title' => Str::random(16),
            'content' => Str::random(16),
            'short_description' => Str::random(16),
            'scheduled_at' => Carbon::today()->format('Y-m-d')
        ]);

        $gig = Gig::first();

        $response = $this->RequestApi('DELETE',
            '/api/gigs/' . $gig->id,$token);

        $this->assertEquals(0, Gig::count());
        $this->assertEquals(200, $response->getStatusCode());
    }

}
