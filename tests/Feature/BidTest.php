<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Support\Str;

class BidTest extends Common
{
    /**
     * Get first 10 records of bids by Gig id.
     * @test
     */
    public function testGetBidsByGigId()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $response = $this->RequestApi('GET',
            "/api/gigs/{$user->id}/bids",$token, []);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('bids', $result->result);
    }

    /**
     *  Allowed to bid by Gig $id.
     */
    public function testPostBidsByGigId()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $response = $this->RequestApi('POST',
        "/api/gigs",$token, [
            'title' => Str::random(16),
            'content' => Str::random(16),
            'short_description' => Str::random(16),
            'scheduled_at' => Carbon::today()->format('Y-m-d')
        ]);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('gig', $result->result);
        $this->assertObjectHasAttribute('id', $result->result->gig);
        $this->assertObjectHasAttribute('title', $result->result->gig);
        $this->assertObjectHasAttribute('content', $result->result->gig);
        $this->assertObjectHasAttribute('short_description', $result->result->gig);
        $this->assertObjectHasAttribute('scheduled_at', $result->result->gig);

        $response = $this->RequestApi('POST',
        "/api/gigs/{$result->result->gig->id}/bids",$token);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('bid', $result->result);
        $this->assertObjectHasAttribute('id', $result->result->bid);
        $this->assertObjectHasAttribute('price', $result->result->bid);
        $this->assertObjectHasAttribute('created_at', $result->result->bid);
    }
}
