<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use App\User;

class Common extends TestCase
{

    public static function passport_install()
    {
        Artisan::call('migrate');
        Artisan::call('passport:install');
    }

    /**
     * @return mixed
     */
    public static function user_register()
    {
        return Passport::register();
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    public static function user_login($email, $password)
    {
        return Passport::register($email, $password);
    }

    /**
     * @param User $user
     * @return string
     */
    public static function get_token(User $user)
    {
        return $user->createToken(env('APP_KEY'))->accessToken;
    }

    /**
     * @param $http_method
     * @param $url
     * @param $token
     * @param array $data
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function RequestApi($http_method, $url, $token, $data = array())
    {
        if('POST' == $http_method) return self::withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->json($http_method, $url, $data);

        if('PUT' == $http_method) return self::withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->json($http_method, $url, $data);

        return self::withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->json($http_method, $url);
    }
}
