<?php

namespace Tests\Feature;

use App\Gig;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ParticipantsTest extends Common
{
    /**
     * Get participants by gig $id
     * @test
     */
    public function get_participants()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $gig = factory(Gig::class)->create([
            'user_id' => $user->id,
            'scheduled_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $gig->participants()->create(['user_id' => rand(10,100)]);

        $response = $this->RequestApi('GET',
            "/api/gigs/{$gig->id}/participants",$token);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('participants', $result->result);
        $this->assertNotEquals(0, $result->result->participants->total);
    }
}
