<?php


namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Carbon\Carbon;

class Passport extends TestCase
{
    public static function register()
    {
        $timestamp = Carbon::now()->timestamp;

        return User::create([
            'first_name' => 'Caesar Ian',
            'last_name' => 'Belza',
            'email' => "test{$timestamp}@domain.com",
            'username' => "test{$timestamp}@domain.com",
            'password' => Hash::make('password')
        ]);
    }

    public static function login($email, $password)
    {
        return self::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
        ])->json('POST', '/api/login', [
            'email' => $email,'password' => $password])->getContent();
    }
}