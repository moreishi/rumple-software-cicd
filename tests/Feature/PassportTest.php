<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use App\User;
use Tests\Feature\Common as Common;

class PassportTest extends Common
{
     use DatabaseMigrations;

    /**
     * A basic passport api register
     */
    public function testRegister()
    {
        self::passport_install();

        $response = $this->RequestAuthApi('POST',
            '/api/register',[
                'first_name' => 'Caesar Ian',
                'last_name' => 'Belza',
                'email' => 'moreishi@gmail.com',
                'username' => 'moreishi@gmail.com',
                'password' => 'password']);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('token', $result->result);
        $this->assertNotEmpty($result->result->token);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLogin()
    {
        self::passport_install();

        User::create([
            'first_name' => 'Caesar Ian',
            'last_name' => 'Belza',
            'email' => 'moreishi@gmail.com',
            'username' => 'moreishi@gmail.com',
            'password' => Hash::make('password')]);

        $response = $this->RequestAuthApi('POST',
            '/api/login',['email' => 'moreishi@gmail.com','password' => 'password']);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
        $this->assertObjectHasAttribute('token', $result->result);
        $this->assertNotEmpty($result->result->token);
    }

    /**
     * @param $http_method
     * @param $url
     * @param array $data
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    public function RequestAuthApi($http_method, $url, $data = [])
    {
        if(count($data) > 0) return self::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
        ])->json($http_method, $url, $data);

        return self::withHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded',
        ])->json($http_method, $url);
    }
}
