<?php

namespace Tests\Feature;

use Tests\Feature\Common as Common;

class UsersTest extends Common
{
    /**
     * Check get users should be logged in.
     */
    public function testGetUsers()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $response = $this->RequestApi('GET',
            '/api/users', $token);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('users', $result->result);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
    }

    /**
     * Get a user should be logged in.
     */
    public function testGetAUser()
    {
        self::passport_install();
        $user = self::user_register();
        $token = self::get_token($user);

        $response = $this->RequestApi('GET',
            '/api/users/' . $user->id, $token);

        $result = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Ok', $result->status);
        $this->assertObjectHasAttribute('user', $result->result);
        $this->assertObjectHasAttribute('status', $result);
        $this->assertObjectHasAttribute('result', $result);
    }

}
